<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;
use Symfony\Component\Yaml\Yaml;




class CacheCommand extends UserCommand
{
    /**
     * {@inheritdoc}
     */
    protected $name = 'cache';
    protected $description = 'Borra caché';
    protected $usage = '/cache';
    protected $version = '0.1';
    /**#@-*/

    /**
     * {@inheritdoc}
     */
    public function execute(): ServerResponse
    {
        $message = $this->getMessage();
        $chat_id = $message->getChat()->getId();
        
        $message = json_decode(strip_tags(trim($message)));

        $values = Yaml::parseFile(dirname(__FILE__) .'/../config/config.yaml');
        

        $username = $message->chat->username;
        if (!in_array($username, $values['telegram_bot']['authorized_usernames'])){
            $data = [
            'chat_id'    => $chat_id,
            'text'       => "sorry, you're not into the authorized users group",
        ];
            return Request::sendMessage($data);
        }

        
        

        $cmd ="drush cr all --root=" . trim($values['site']['site_root']);

        $out = `$cmd`;
      
        $data = [
            'chat_id'    => $chat_id,
            'text'       => "Cache Rebuild complete at ". date("D M Y - H:m"),
        ];

        return Request::sendMessage($data);
    }
    
}
