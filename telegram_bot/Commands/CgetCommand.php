<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;
use Symfony\Component\Yaml\Yaml;


class CgetCommand extends UserCommand
{
    /**
     * {@inheritdoc}
     */
    protected $name = 'cget';
    protected $description = 'Obtiene valores de configuración';
    protected $usage = '/cget';
    protected $version = '0.1';
    /**#@-*/

    /**
     * {@inheritdoc}
     */
    public function execute(): ServerResponse
    {
        $message = $this->getMessage();
        $chat_id = $message->getChat()->getId();
        
        $message = json_decode(strip_tags(trim($message)));
        $ar_msg  = $message->text;
        $ar_msg  = str_replace("/cget", "", $ar_msg);

        $values = Yaml::parseFile(dirname(__FILE__) .'/../config/config.yaml');
        

        $username = $message->chat->username;
        if (!in_array($username, $values['telegram_bot']['authorized_usernames'])){
            $data = [
            'chat_id'    => $chat_id,
            'text'       => "sorry, you're not into the authorized users group",
        ];
            return Request::sendMessage($data);
        }

        

        $cmd ="cd ". trim($values['site']['site_root']) ."; drush cget ". $ar_msg ." --format=yaml";
        //print_r($cmd);

        $out = `$cmd`;
      
        $data = [
            'chat_id'    => $chat_id,
            'text'       => $out,
        ];

        return Request::sendMessage($data);
    }
}
