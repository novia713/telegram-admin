<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;
use Symfony\Component\Yaml\Yaml;

/**
 * User "/markdown" command
 */
class CronCommand extends UserCommand
{
    /**#@+
     * {@inheritdoc}
     */
    protected $name = 'cron';
    protected $description = 'Ejecuta el cron';
    protected $usage = '/cron';
    protected $version = '0.1';


    /**
     * {@inheritdoc}
     */
    public function execute(): ServerResponse
    {
        $message = $this->getMessage();
        $chat_id = $message->getChat()->getId();
        
        $message = json_decode(strip_tags(trim($message)));
        $ar_msg  = $message->text;

        $values = Yaml::parseFile(dirname(__FILE__) .'/../config/config.yaml');



        $username = $message->chat->username;
        if (!in_array($username, $values['telegram_bot']['authorized_usernames'])){
            $data = [
            'chat_id'    => $chat_id,
            'text'       => "sorry, you're not into the authorized users group",
        ];
            return Request::sendMessage($data);
        }


        

        $cmd ="cd ". trim($values['site']['site_root']) ."; drush cron -y";

        $out = `$cmd`;
      
        $data = [
            'chat_id'    => $chat_id,
            'text'       => $out,
        ];

        return Request::sendMessage($data);
    }
}
