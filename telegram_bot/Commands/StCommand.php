<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;
use Symfony\Component\Yaml\Yaml;


 
class StCommand extends UserCommand
{
    /**
     * {@inheritdoc}
     */
    protected $name = 'st';
    protected $description = 'Hace un status del sitio';
    protected $usage = '/st';
    protected $version = '0.1';
    /**#@-*/

    /**
     * {@inheritdoc}
     */
    public function execute(): ServerResponse
    {
        $message = $this->getMessage();
        $chat_id = $message->getChat()->getId();
        
        $message = json_decode(strip_tags(trim($message)));
        

        $values = Yaml::parseFile(dirname(__FILE__) .'/../config/config.yaml');


        $cmd ="cd ".trim($values['site']['site_root'])."; drush st";

        $out = `$cmd`;

        $username = $message->chat->username;
        if (!in_array($username, $values['telegram_bot']['authorized_usernames'])){
            $data = [
            'chat_id'    => $chat_id,
            'text'       => "sorry, you're not into the authorized users group",
        ];
            return Request::sendMessage($data);
        }
      
        $data = [
            'chat_id'    => $chat_id,
            'text'       => $out,
        ];

        return Request::sendMessage($data);
    }
}
