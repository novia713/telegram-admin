<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Entities\ReplyKeyboardMarkup;

/**
 * User "/markdown" command
 */
class HelpCommand extends UserCommand
{
    /**
     * {@inheritdoc}
     */
    protected $name = 'help';
    protected $description = 'Muestra los comandos disponibles';
    protected $usage = '/help';
    protected $version = '0.1';


    /**
     * {@inheritdoc}
     */
    public function execute(): ServerResponse
    {
        $message = $this->getMessage();
        $chat_id = $message->getChat()->getId();

        $data = [
            'chat_id'    => $chat_id,
            'parse_mode' => 'MARKDOWN',
            'text'       => '*Ayuda de _Drupalillo_*

 Envía /help para consultar esta ayuda y los comandos disponibles.
 
 - /cache : Vacía la caché del sitio
 - /cget: Información sobre valores de configuración [ _/cget system.site_ ]
 - /cron: Ejecuta el cron
 - /st: drush status
 - /updb: actualiza la base de datos
',
        ];

        return Request::sendMessage($data);
    }
}
