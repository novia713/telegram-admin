<?php
use Symfony\Component\Yaml\Yaml;


date_default_timezone_set('Europe/Madrid');

// Bash script
// while true; do ./getUpdatesCLI.php; done



// Load composer
require dirname(__FILE__). '/../vendor/autoload.php';
 
$values = Yaml::parseFile(dirname(__FILE__) .'/config/config.yaml');
// Add you bot's API key and name
$API_KEY  = trim($values['telegram_bot']['api_key']);
$BOT_NAME = trim($values['telegram_bot']['bot_name']);

// Define a path for your custom commands
$commands_path = dirname(__FILE__) . '/Commands/';


try {
    // Create Telegram API object
    $telegram = new Longman\TelegramBot\Telegram($API_KEY, $BOT_NAME);

$telegram->useGetUpdatesWithoutDatabase();



    // Add an additional commands path
    $telegram->addCommandsPath($commands_path);

    // Enable admin user(s)bo
    $telegram->enableAdmin(5826301);


    // Handle telegram getUpdates request
    $serverResponse = $telegram->handleGetUpdates();
    

    if ($serverResponse->isOk()) {
        $updateCount = count($serverResponse->getResult());
        echo date('Y-m-d H:i:s', time()) . ' - Processed ' . $updateCount . ' updates';
    } else {
        echo date('Y-m-d H:i:s', time()) . ' - Failed to fetch updates' . PHP_EOL;
        echo $serverResponse->printError();
    }
} catch (Longman\TelegramBot\Exception\TelegramException $e) {
    echo $e;
    // Log telegram errors
    Longman\TelegramBot\TelegramLog::error($e);
} catch (Longman\TelegramBot\Exception\TelegramLogException $e) {
    // Catch log initilization errors
    echo $e;
}
